<?php

return [
    'ID' => 'ID',
    'Name' => 'Название',

    'Birth Date Type' => 'Тип условия на дату рождения',
    'Is Phone Filled' => 'Учитывать заполненность телефона',
    'Phone Last Digits' => 'Последние цифры телефона',
    'Gender' => 'Пол',
    'Active Date From' => 'Активно с',
    'Active Date To' => 'Активно по',
    'Discount' => 'Скидка',
    'Is Active' => 'Активен',

    'Fio' => 'ФИО',
    'Birth Date' => 'Дата рождения',
    'Phone' => 'Телефон',
    'Created From' => 'Создано с',
    'Created To' => 'Создано по',

    'Create' => 'Создать',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'Search' => 'Поиск',

    'Create Discount Condition' => 'Создать условие скидки',
    'Discount Conditions' => 'Условия скидки',

    'Filter' => 'Фильтр',
    'Reset filter' => 'Сбросить фильтр',

    'Update Discount Condition: {modelName}' => 'Обновить условие скидки: {modelName}',

    'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить этот объект?',

    'Home' => 'Главная',
    'Admin panel' => 'Админ-панель',
    'Login' => 'Вход',
    'Services' => 'Услуги',
    'Orders' => 'Заказы',
    'Discounts' => 'Скидки',
    'User management' => 'Пользователи',
    'Site' => 'Сайт',
    'Logout' => 'Выход',

    'Calc discount' => 'Рассчитать скидку',

    'Create Order' => 'Создать заказ',
    'Update Order: {modelName}' => 'Обновить заказ: {modelName}',

    'Order' => 'Заказ',
    'Discount Condition' => 'Условие скидки',

    'Create Service' => 'Создать услугу',
    'Update Service: {modelName}' => 'Обновить услугу: {modelName}',

    'Cannot save record' => 'Не удалось сохранить запись',

    'Select...' => 'Выбрать...',

    'Service' => 'Услуга',

    'Male' => 'Мужской',
    'Female' => 'Женский',

    'Created At' => 'Создано',

    'No discount' => 'Нет скидки',
];

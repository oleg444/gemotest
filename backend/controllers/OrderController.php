<?php

namespace backend\controllers;

use Yii;
use common\base\CrudController;
use common\models\Order;
use backend\models\OrderSearch;
use yii\helpers\ArrayHelper;
use common\components\UnitOfWork\UnitOfWork;
use yii\db\ActiveRecord;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends CrudController
{
    /**
     * @inheritdoc
     */
    public function init()
	{
		$this->modelClass = Order::className();
		$this->searchModelClass = OrderSearch::className();
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), []);
    }

    /**
     * Get Discount
     *
     * @return mixed|null|string
     */
    public function actionGetDiscount()
    {
        $unitOfWork = new UnitOfWork();
        $postData = Yii::$app->request->post();
        $model = new Order();

        if ($postData && $this->loadModel($unitOfWork, $model, $postData)) {

            $discount = $model->getDiscount();
            if (!$discount) {
                $discount = Yii::t('app', 'No discount');
            } else {
                $isPercent = (substr_compare($discount, '%', strlen($discount) - 1) === 0);
                $discount .= ($isPercent ? '' : ' р.');
            }

            return $discount;
        }

        return '';
    }

    /**
     * @inheritdoc
     */
    protected function loadModel(UnitOfWork $unitOfWork, ActiveRecord $model, $postData)
    {
        // fix for data from checkboxes
        if (isset($postData['Order']['services'])) {
            $servicesData = $postData['Order']['services'];
            $servicesData = array_filter($servicesData);
            $postData['Order']['services'] = array_keys($servicesData);
        }

        return parent::loadModel($unitOfWork, $model, $postData);
    }
}

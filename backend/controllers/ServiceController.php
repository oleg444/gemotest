<?php

namespace backend\controllers;

use Yii;
use common\base\CrudController;
use common\models\Service;
use backend\models\ServiceSearch;
use yii\helpers\ArrayHelper;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends CrudController
{
    /**
     * @inheritdoc
     */
    public function init()
	{
		$this->modelClass = Service::className();
		$this->searchModelClass = ServiceSearch::className();
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), []);
    }
}

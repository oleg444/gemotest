<?php

namespace backend\controllers;

use Yii;
use common\base\CrudController;
use common\models\DiscountCondition;
use backend\models\DiscountConditionSearch;
use yii\helpers\ArrayHelper;
use common\components\UnitOfWork\UnitOfWork;
use yii\db\ActiveRecord;

/**
 * DiscountConditionController implements the CRUD actions for DiscountCondition model.
 */
class DiscountConditionController extends CrudController
{
    /**
     * @inheritdoc
     */
    public function init()
	{
		$this->modelClass = DiscountCondition::className();
		$this->searchModelClass = DiscountConditionSearch::className();
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), []);
    }

    /**
     * @inheritdoc
     */
    protected function loadModel(UnitOfWork $unitOfWork, ActiveRecord $model, $postData)
    {
        // fix for data from checkboxes
        if (isset($postData['DiscountCondition']['services'])) {
            $servicesData = $postData['DiscountCondition']['services'];
            $servicesData = array_filter($servicesData);
            $postData['DiscountCondition']['services'] = array_keys($servicesData);
        }

        return parent::loadModel($unitOfWork, $model, $postData);
    }
}

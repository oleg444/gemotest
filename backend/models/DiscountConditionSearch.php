<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DiscountCondition;

/**
 * DiscountConditionSearch represents the model behind the search form about `common\models\DiscountCondition`.
 */
class DiscountConditionSearch extends Model
{
    public $id;
    public $birth_date_type_id;
    public $is_phone_filled;
    public $phone_last_digits;
    public $gender;
    public $active_date_from;
    public $active_date_to;
    public $discount;
    public $is_active;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'birth_date_type_id'], 'integer'],
            [['is_phone_filled', 'is_active'], 'boolean'],
            [['phone_last_digits', 'gender', 'active_date_from', 'active_date_to', 'discount'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'birth_date_type_id' => Yii::t('app', 'Birth Date Type'),
            'is_phone_filled' => Yii::t('app', 'Is Phone Filled'),
            'phone_last_digits' => Yii::t('app', 'Phone Last Digits'),
            'gender' => Yii::t('app', 'Gender'),
            'active_date_from' => Yii::t('app', 'Active Date From'),
            'active_date_to' => Yii::t('app', 'Active Date To'),
            'discount' => Yii::t('app', 'Discount'),
            'is_active' => Yii::t('app', 'Is Active'),

            'birthDateType.name' => Yii::t('app', 'Birth Date Type'),
        ];
    }

    /**
     * Checks if the filter panel should be showed as open
     *
     * @return bool Returns true if any search attribute is filled
     */
    public function isOpen()
    {
        $attributes = $this->safeAttributes();
        foreach ($attributes as $attribute) {
            if (!empty($this->$attribute)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DiscountCondition::find();
        $query->joinWith(['birthDateType']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['birthDateType.name'] = [
            'asc'  => ['birth_date_type.name' => SORT_ASC],
            'desc' => ['birth_date_type.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['genderName'] = [
            'asc'  => ['gender' => SORT_ASC],
            'desc' => ['gender' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'discount_condition.id' => $this->id,
            'discount_condition.birth_date_type_id' => $this->birth_date_type_id,
            'discount_condition.is_phone_filled' => $this->is_phone_filled,
            'discount_condition.phone_last_digits' => $this->phone_last_digits,
            'discount_condition.active_date_from' => $this->active_date_from,
            'discount_condition.active_date_to' => $this->active_date_to,
            'discount_condition.is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'discount_condition.phone_last_digits', $this->phone_last_digits]);
        $query->andFilterWhere(['like', 'discount_condition.gender', $this->gender]);
        $query->andFilterWhere(['like', 'discount_condition.discount', $this->discount]);

        return $dataProvider;
    }
}

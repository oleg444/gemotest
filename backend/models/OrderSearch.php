<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Model
{
    public $id;
    public $fio;
    public $birth_date;
    public $phone;
    public $gender;
    public $created_from;
    public $created_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fio', 'birth_date', 'phone', 'gender', 'created_from', 'created_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fio' => Yii::t('app', 'Fio'),
            'birth_date' => Yii::t('app', 'Birth Date'),
            'phone' => Yii::t('app', 'Phone'),
            'gender' => Yii::t('app', 'Gender'),
            'created_from' => Yii::t('app', 'Created From'),
            'created_to' => Yii::t('app', 'Created To'),
        ];
    }

    /**
     * Checks if the filter panel should be showed as open
     *
     * @return bool Returns true if any search attribute is filled
     */
    public function isOpen()
    {
        $attributes = $this->safeAttributes();
        foreach ($attributes as $attribute) {
            if (!empty($this->$attribute)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['genderName'] = [
            'asc'  => ['gender' => SORT_ASC],
            'desc' => ['gender' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'order.id' => $this->id,
            'order.birth_date' => $this->birth_date,
        ]);
        $query->andFilterWhere(['>=', 'offers.created_at', $this->created_from]);
        $query->andFilterWhere(['<=', 'offers.created_at', $this->created_to]);

        $query->andFilterWhere(['like', 'order.fio', $this->fio])
            ->andFilterWhere(['like', 'order.phone', $this->phone])
            ->andFilterWhere(['like', 'order.gender', $this->gender]);

        return $dataProvider;
    }
}

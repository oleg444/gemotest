<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use common\helpers\UrlHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = Yii::t('app', 'Order') . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => UrlHelper::previous()];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <div class="page-title">
        <div class="row">
            <div class="col-sm-8">
                <h2><?= Html::encode($this->title) ?></h2>
            </div>

            <div class="col-sm-4">
                <div class="pull-right">
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            [
                'attribute' => 'services',
                'format' => 'html',
                'value' => implode('<br>', ArrayHelper::map($model->services, 'id', 'name')),
            ],
            'birth_date:date',
            'phone',
            'genderName',
            'discount',
            'created_at:datetime',
        ],
    ]) ?>

    <?php if ($model->discount_condition_id) { ?>
        <br>
        <h4><?= Yii::t('app', 'Discount Condition') ?></h4>

        <?= DetailView::widget([
            'model' => $model->discountCondition,
            'attributes' => [
                'id',
                [
                    'attribute' => 'services',
                    'format' => 'html',
                    'value' => implode('<br>', ArrayHelper::map($model->services, 'id', 'name')),
                ],
                'birthDateType.name',
                'is_phone_filled:boolean',
                'phone_last_digits',
                'genderName',
                'active_date_from:datetime',
                'active_date_to:datetime',
                'discount',
                'is_active:boolean',
            ],
        ]) ?>
    <?php } ?>

</div>

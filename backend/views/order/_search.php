<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\Render;
use common\models\Gender;

/* @var $this yii\web\View */
/* @var $model backend\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php
        $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'enableClientValidation' => false,
        ]);

        $render = new Render($form, $model);
    ?>

    <div class="row">

        <div class="col-sm-3">
            <?= $form->field($model, 'id') ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'fio') ?>
        </div>

        <div class="col-sm-3">
            <?= $render->dateField('birth_date') ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'phone') ?>
        </div>

        <div class="col-sm-3">
            <?= $render->selectField('gender', Gender::getList()); ?>
        </div>

        <div class="col-sm-3">
            <?= $render->dateTimeField('created_from') ?>
        </div>

        <div class="col-sm-3">
            <?= $render->dateTimeField('created_to') ?>
        </div>

    </div>

    <div class="form-group no-margin">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

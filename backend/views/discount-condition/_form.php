<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\Render;
use common\models\BirthDateType;
use common\models\Gender;
use common\models\Service;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountCondition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discount-condition-form">

    <?php
        $form = ActiveForm::begin();

        $render = new Render($form, $model);
    ?>

    <div>
        <label><?= Yii::t('app', 'Services') ?></label>
        <br>
        <br>

        <?php foreach (Service::find()->where(['is_active' => 1])->all() as $service) { ?>
            <?= $render->checkboxField('services['.$service->id.']', [
                    'options' => ['value' => isset($model->services[$service->id])]
                ])
                ->label($service->name)
            ?>
        <?php } ?>

        <br>
    </div>

    <?= $render->selectField('birth_date_type_id', BirthDateType::getList()) ?>

    <div>
        <br>
        <?= $render->checkboxField('is_phone_filled') ?>
        <br>
    </div>

    <?= $form->field($model, 'phone_last_digits')->textInput(['maxlength' => true]) ?>

    <?= $render->selectField('gender', Gender::getList()); ?>

    <?= $render->dateTimeField('active_date_from') ?>

    <?= $render->dateTimeField('active_date_to') ?>

    <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

    <?= $render->checkboxField('is_active') ?>
    <div class="form-group m-t-md">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

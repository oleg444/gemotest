<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\Render;
use common\models\BirthDateType;
use common\models\Gender;

/* @var $this yii\web\View */
/* @var $model backend\models\DiscountConditionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discount-condition-search">

    <?php
        $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'enableClientValidation' => false,
        ]);

        $render = new Render($form, $model);
    ?>

    <div class="row">

        <div class="col-sm-3">
            <?= $form->field($model, 'id') ?>
        </div>

        <div class="col-sm-3">
            <?= $render->selectField('birth_date_type_id', BirthDateType::getList()) ?>
        </div>

        <div class="col-sm-3">
            <?= $render->checkboxField('is_phone_filled') ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'phone_last_digits') ?>
        </div>

        <div class="col-sm-3">
            <?= $render->selectField('gender', Gender::getList()); ?>
        </div>

        <div class="col-sm-3">
            <?= $render->dateField('active_date_from') ?>
        </div>

        <div class="col-sm-3">
            <?= $render->dateField('active_date_to') ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'discount') ?>
        </div>

        <div class="col-sm-3">
            <?= $render->checkboxField('is_active') ?>
        </div>

    </div>

    <div class="form-group no-margin">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

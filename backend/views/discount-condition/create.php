<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\UrlHelper;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountCondition */

$model->is_active = 1;
$model->is_phone_filled = 0;

$this->title = Yii::t('app', 'Create Discount Condition');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Discount Conditions'), 'url' => UrlHelper::previous()];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-condition-create">

    <div class="page-title">
        <h2><?= Html::encode($this->title) ?></h2>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\UrlHelper;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountCondition */

$model->is_phone_filled = (int)$model->is_phone_filled;

$this->title = Yii::t('app', 'Update Discount Condition: {modelName}', [
    'modelName' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Discount Conditions'), 'url' => UrlHelper::previous()];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="discount-condition-update">

    <div class="page-title">
        <h2><?= Html::encode($this->title) ?></h2>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

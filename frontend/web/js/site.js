$(document).ready(function() {

    $('.input-group-focus-button').click(function() {
        $(this).closest('.input-group').find('input').first().focus();
    });

    $('body').on('click', '.file-drop-zone > .file-drop-zone-title, .file-drop-zone > .clearfix', function(e) {
        if (e.target == this) {
            $(this).closest('.file-input').find('input[type=file]').trigger('click');
        }
    });

});

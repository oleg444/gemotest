<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\Render;
use common\models\Gender;
use common\models\Service;
use common\widgets\Script;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php
        $form = ActiveForm::begin(['id' => 'create-order-form']);

        $render = new Render($form, $model);
    ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <div>
        <label><?= Yii::t('app', 'Services') ?></label>
        <br>
        <br>

        <?php foreach (Service::find()->where(['is_active' => 1])->all() as $service) { ?>
            <?= $render->checkboxField('services['.$service->id.']', [
                    'options' => ['value' => isset($model->services[$service->id])]
                ])
                ->label($service->name)
            ?>
        <?php } ?>

        <br>
    </div>

    <?= $render->dateField('birth_date') ?>

    <?= $form->field($model, 'phone', [
            'template' => '
                {label}
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1">+7</span>
                  {input}
                </div>
                {hint}
                {error}
            '
        ])->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '(999) 999-99-99',
        ])
    ?>

    <?= $render->selectField('gender', Gender::getList()); ?>

    <div class="form-group m-t-md">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?php if ($model->isNewRecord) { ?>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?= Html::button(
                Yii::t('app', 'Calc discount'),
                ['class' => 'btn btn-primary calc_discount_btn', 'data-url' => Url::to('order/get-discount')]
            ) ?>
            &nbsp;
            <span class="discount_value"></span>
        <?php } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php if ($model->isNewRecord) { ?>
    <?php Script::begin(); ?>
        <script>
            $('.calc_discount_btn').click(function(){
                var $btn = $(this);
                $btn.prop('disabled', true);

                $.post(
                    $(this).data('url'),
                    $('input, select, textarea', '#create-order-form').serialize()
                ).success(function (data) {
                    $btn.prop('disabled', false);
                    $('.discount_value').html(data);
                }).error(function (data) {
                    $btn.prop('disabled', false);
                });
            });
        </script>
    <?php Script::end(); ?>
<?php } ?>

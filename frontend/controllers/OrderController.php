<?php

namespace frontend\controllers;

use Yii;
use backend\controllers\OrderController as BaseOrderController;
use common\base\CrudController;
use common\models\Order;
use yii\helpers\ArrayHelper;
use common\components\UnitOfWork\UnitOfWork;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends BaseOrderController
{
    /**
     * @inheritdoc
     */
    public function init()
	{
		$this->modelClass = Order::className();
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    public function actionIndex()
    {
        throw new NotFoundHttpException();
    }

    public function actionUpdate($id)
    {
        throw new NotFoundHttpException();
    }

    public function actionDelete($id)
    {
        throw new NotFoundHttpException();
    }
}

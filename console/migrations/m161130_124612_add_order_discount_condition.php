<?php

use yii\db\Schema;

class m161130_124612_add_order_discount_condition extends yii\db\Migration
{
    public function up()
    {
        $this->addColumn('{{%order}}', 'discount_condition_id', $this->integer()->after('gender'));
        $this->createIndex('FK_order___discount_condition', '{{%order}}', 'discount_condition_id', false);
        $this->addForeignKey('FK_order___discount_condition', '{{%order}}', 'discount_condition_id', '{{%discount_condition}}', 'id', null, null);
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->dropForeignKey('FK_order___discount_condition', '{{%order}}');
        $this->dropIndex('FK_order___discount_condition', '{{%order}}');
        $this->dropColumn('{{%order}}', 'discount_condition_id');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }
}

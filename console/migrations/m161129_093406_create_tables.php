<?php

use yii\db\Schema;

class m161129_093406_create_tables extends yii\db\Migration
{
    public function up()
    {
        $this->createTable('{{%birth_date_type}}', [
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(100)->notNull(),
        ]);


        $this->createTable('{{%discount_condition}}', [
            'id' => $this->primaryKey()->notNull(),
            'birth_date_type_id' => $this->integer(),
            'is_phone_filled' => $this->boolean(1)->defaultValue('0')->notNull(),
            'phone_last_digits' => $this->string(30),
            'gender' => $this->getDb()->getSchema()->createColumnSchemaBuilder('CHAR(1)'),
            'active_date_from' => $this->timestamp()->notNull(),
            'active_date_to' => $this->timestamp(),
            'discount' => $this->string(20)->notNull(),
            'is_active' => $this->boolean(1)->defaultValue('1')->notNull(),
        ]);

        $this->createIndex('active_date_from', '{{%discount_condition}}', 'active_date_from', false);
        $this->createIndex('active_date_to', '{{%discount_condition}}', 'active_date_to', false);
        $this->createIndex('FK_discount_condition___birth_date_type', '{{%discount_condition}}', 'birth_date_type_id', false);


        $this->createTable('{{%discount_condition__service}}', [
            'discount_condition_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('PRIMARY KEY', '{{%discount_condition__service}}', ['discount_condition_id', 'service_id']);

        $this->createIndex('FK_discount_condition__service___service', '{{%discount_condition__service}}', 'service_id', false);


        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey()->notNull(),
            'fio' => $this->string(100)->notNull(),
            'birth_date' => $this->date()->notNull(),
            'phone' => $this->string(30),
            'gender' => $this->getDb()->getSchema()->createColumnSchemaBuilder('CHAR(1)'),
            'created_at' => $this->timestamp()->notNull(),
        ]);

        $this->createIndex('date_create', '{{%order}}', 'created_at', false);


        $this->createTable('{{%order__service}}', [
            'order_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('PRIMARY KEY', '{{%order__service}}', ['order_id', 'service_id']);

        $this->createIndex('FK_order__service___service', '{{%order__service}}', 'service_id', false);


        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(100)->notNull(),
            'is_active' => $this->boolean(1)->defaultValue('1')->notNull(),
        ]);


        $this->addForeignKey('FK_discount_condition___birth_date_type', '{{%discount_condition}}', 'birth_date_type_id', '{{%birth_date_type}}', 'id', null, null);

        $this->addForeignKey('FK_discount_condition__service___service', '{{%discount_condition__service}}', 'service_id', '{{%service}}', 'id', null, null);
        $this->addForeignKey('FK_discount_condition__service___discount_condition', '{{%discount_condition__service}}', 'discount_condition_id', '{{%discount_condition}}', 'id', 'CASCADE', null);

        $this->addForeignKey('FK_order__service___service', '{{%order__service}}', 'service_id', '{{%service}}', 'id', null, null);
        $this->addForeignKey('FK_order__service___order', '{{%order__service}}', 'order_id', '{{%order}}', 'id', 'CASCADE', null);


        $data = [
            ['id' => '1', 'name' => 'Неделя до дня рождения'],
            ['id' => '2', 'name' => 'Неделя после дня рождения'],
            ['id' => '3', 'name' => 'Неделя до и после дня рождения'],
        ];
        $this->batchInsert('{{%birth_date_type}}', [], $data);
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');

        $this->dropTable('{{%service}}');
        $this->dropTable('{{%order__service}}');
        $this->dropTable('{{%order}}');
        $this->dropTable('{{%discount_condition__service}}');
        $this->dropTable('{{%discount_condition}}');
        $this->dropTable('{{%birth_date_type}}');

        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }
}

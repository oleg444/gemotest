<?php

namespace common\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\FormatConverter;

/**
 * Extended DateTimePicker, allows to set different formats for sending and displaying value
 */
class DateTimePicker extends \kartik\datetime\DateTimePicker
{
    public $saveDateFormat = 'php:Y-m-d H:i';
    public $removeButtonSelector = '.kv-date-remove';

    private $savedValueInputID = '';
    private $sourceValue = null;


    public function init()
    {
        if ($this->hasModel()) {
            $model = $this->model;
            $attribute = $this->attribute;
            $value = Html::getAttributeValue($model, $attribute);

            $this->model = null;
            $this->attribute = null;
            $this->options['id'] = Html::getInputId($model, $attribute);
            $this->name = Html::getInputName($model, $attribute);
            $this->sourceValue = $value;

            if ($value) {
                $this->value = Yii::$app->formatter->asDateTime($value, $this->pluginOptions['format']);
            }
        }

        return parent::init();
    }



    public function registerAssets()
    {
        if ($this->disabled) {
            return;
        }
        $view = $this->getView();
        if (!empty($this->_langFile)) {
            \kartik\datetime\DateTimePickerAsset::register($view)->js[] = $this->_langFile;
        } else {
            \kartik\datetime\DateTimePickerAsset::register($view);
        }
        if ($this->type == self::TYPE_INLINE) {
            $this->pluginOptions['linkField'] = $this->options['id'];
            if (!empty($this->pluginOptions['format'])) {
                $this->pluginOptions['linkFormat'] = $this->pluginOptions['format'];
            }
        }
        if ($this->type == self::TYPE_INPUT) {
            $this->registerPlugin($this->pluginName);
        } else {
            $this->registerPlugin($this->pluginName, 'jQuery(this)');
        }
    }


    protected function registerPlugin($name, $element = null, $callback = null, $callbackCon = null)
    {
        $script = $this->getPluginScript($name, $element, $callback, $callbackCon);
        $this->registerWidgetJs($script, \yii\web\View::POS_END);
    }

    protected function getPluginScript($name, $element = null, $callback = null, $callbackCon = null)
    {
        $defaultInitScript = parent::getPluginScript($name, $element, $callback, $callbackCon);
        $additionalScript = $this->getAdditionalScript();

        $script = $defaultInitScript . $additionalScript;

        $hash = hash('crc32', $defaultInitScript);
        $initFunctionName = $name . '_init_' . $hash;
        $script = "function $initFunctionName() {
                $script
            }
        ";

        $this->options['data-widget-init'] = $initFunctionName;
        return $script;
    }


    protected function initDestroyJs()
    {
        if (isset($this->pluginDestroyJs)) {
            return;
        }
        if (empty($this->pluginName)) {
            $this->pluginDestroyJs = '';
            return;
        }
        $id = "jQuery(this)";
        $plugin = $this->pluginName;
        $this->pluginDestroyJs = "if ({$id}.data('{$this->pluginName}')) { {$id}.{$plugin}('destroy'); }";
    }



    protected function parseMarkup($input)
    {
        $res = parent::parseMarkup($input);

        $res .= $this->renderSavedValueInput();

        return $res;
    }

    protected function renderSavedValueInput()
    {
        $value = $this->sourceValue;

        if ($value !== null && $value !== '') {
            // format value according to saveDateFormat
            try {
                $value = Yii::$app->formatter->asDateTime($value, $this->saveDateFormat);
            } catch(InvalidParamException $e) {
                // ignore exception and keep original value if it is not a valid date
            }
        }

        $options = [];
        $options['class'] = 'datetimepicker-saved-value';
        $options['value'] = $value;

        // render hidden input
        if ($this->hasModel()) {
            $contents = Html::activeHiddenInput($this->model, $this->attribute, $options);
        } else {
            $contents = Html::hiddenInput($this->name, $value, $options);
        }

        return $contents;
    }

    protected function getAdditionalScript()
    {
        $format = $this->saveDateFormat;
        $format = strncmp($format, 'php:', 4) === 0
            ? substr($format, 4)
            : FormatConverter::convertDateIcuToPhp($format, 'datetime');
        $saveDateFormatJs = static::convertDateFormat($format);


        $script = "
            var container = $(this);
            container.closest('.date').find('{$this->removeButtonSelector}').on('click', function(e) {
                container.find('input').val('').trigger('change');
                container.data('datetimepicker').reset();
            });

            var hiddenInput = $(this).closest('.form-group').find('.datetimepicker-saved-value');
            container.on('changeDate', function(e) {
                var datetimepicker = $(e.target).data('datetimepicker');
                var savedValue = datetimepicker.getFormattedDate($.fn.datetimepicker.DPGlobal.parseFormat('{$saveDateFormatJs}', 'standard'));
                hiddenInput.val(savedValue).trigger('change');
            });
        ";

        return $script;
    }
}

<?php

namespace common\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\FormatConverter;

/**
 * Extended DatePicker, allows to set different formats for sending and displaying value
 */
class DatePicker extends \kartik\date\DatePicker
{
    public $saveDateFormat = 'php:Y-m-d';

    private $savedValueInputID = '';
    private $sourceValue = null;


    public function init()
    {
        if ($this->hasModel()) {
            $model = $this->model;
            $attribute = $this->attribute;
            $value = Html::getAttributeValue($model, $attribute);

            $this->model = null;
            $this->attribute = null;
            $this->options['id'] = Html::getInputId($model, $attribute);
            $this->name = Html::getInputName($model, $attribute);
            $this->sourceValue = $value;

            if ($value) {
                $this->value = Yii::$app->formatter->asDate($value, $this->pluginOptions['format']);
            }
        }

        $res = parent::init();
        return $res;
    }


    public function registerAssets()
    {
        if ($this->disabled) {
            return;
        }
        $view = $this->getView();
        $view->registerJs('jQuery.fn.kvDatepicker.dates={};');
        if (!empty($this->_langFile)) {
            \kartik\date\DatePickerAsset::register($view)->js[] = $this->_langFile;
        } else {
            \kartik\date\DatePickerAsset::register($view);
        }

        $input = "jQuery(this)";
        $el = $input . ".closest('.date')";
        $this->registerPlugin($this->pluginName, $el);
    }

    protected function registerPlugin($name, $element = null, $callback = null, $callbackCon = null)
    {
        $script = $this->getPluginScript($name, $element, $callback, $callbackCon);
        $this->registerWidgetJs($script, \yii\web\View::POS_END);
    }

    protected function getPluginScript($name, $element = null, $callback = null, $callbackCon = null)
    {
        $defaultInitScript = parent::getPluginScript($name, $element, $callback, $callbackCon);


        if ($this->type === self::TYPE_INLINE) {
            $defaultInitScript .= "{$element}.on('changeDate',function(e){jQuery(this).val(e.format()).trigger('change')});";
        }
        if ($this->_hasAddon && $this->removeButton !== false) {
            $defaultInitScript .= "
                var initDPRemove = function (\$id, range) {
                    var \$el = \$id.parent(), \$input;
                    \$el.find('.kv-date-remove').on('click.kvdatepicker', function () {
                        if (range) {
                            \$el.find('input[type=\\\"text\\\"]').each(function () {
                                $(this).kvDatepicker('clearDates').trigger('change');
                            });
                        } else {
                            \$el.kvDatepicker('clearDates');
                            \$input = \$el.is('input') ? \$el : \$el.find('input[type=\\\"text\\\"]');
                            \$input.trigger('change');
                        }
                    });
                };
                initDPRemove($element.find('input'));
            ";
        }
        if ($this->_hasAddon) {
            $defaultInitScript .= "
                var initDPAddon = function (\$id) {
                    var \$el = \$id.parent();
                    \$el.find('.input-group-addon:not(.kv-date-calendar):not(.kv-date-remove)').each(function () {
                        var \$addon = $(this);
                        \$addon.on('click.kvdatepicker', function () {
                            \$el.kvDatepicker('hide');
                        });
                    });
                    \$el.find('.input-group-addon.kv-date-calendar').on('click.kvdatepicker', function () {
                        \$id.focus();
                    });
                };
                initDPAddon($element);
            ";
        }



        $additionalInitScript = $this->getAdditionalInitScript();
        $script = $defaultInitScript . "\n" . $additionalInitScript;

        $hash = hash('crc32', $defaultInitScript);
        $initFunctionName = $name . '_init_' . $hash;
        $script = "function $initFunctionName() {
                $script
            }
        ";

        $this->options['data-widget-init'] = $initFunctionName;
        return $script;
    }


    protected function initDestroyJs()
    {
        if (isset($this->pluginDestroyJs)) {
            return;
        }
        if (empty($this->pluginName)) {
            $this->pluginDestroyJs = '';
            return;
        }
        $id = "jQuery(this)";
        $plugin = $this->pluginName;
        $this->pluginDestroyJs = "if ({$id}.data('{$this->pluginName}')) { {$id}.{$plugin}('destroy'); }";
    }


    protected function parseMarkup($input)
    {
        $res = parent::parseMarkup($input);
        $res .= $this->renderSavedValueInput();
        return $res;
    }

    protected function renderSavedValueInput()
    {
        $value = $this->sourceValue;

        if ($value !== null && $value !== '') {
            // format value according to saveDateFormat
            try {
                $value = Yii::$app->formatter->asDate($value, $this->saveDateFormat);
            } catch(InvalidParamException $e) {
                // ignore exception and keep original value if it is not a valid date
            }
        }

        $options = [];
        $options['class'] = 'datepicker-saved-value';
        $options['value'] = $value;

        // render hidden input
        if ($this->hasModel()) {
            $contents = Html::activeHiddenInput($this->model, $this->attribute, $options);
        } else {
            $contents = Html::hiddenInput($this->name, $value, $options);
        }

        return $contents;
    }

    protected function getAdditionalInitScript()
    {
        $language = $this->language ? $this->language : Yii::$app->language;

        $format = $this->saveDateFormat;
        $format = strncmp($format, 'php:', 4) === 0 ? substr($format, 4) :
            FormatConverter::convertDateIcuToPhp($format, 'date');
        $saveDateFormatJs = static::convertDateFormat($format);


        $script = "
            var container = $(this).closest('.date');
            var hiddenInput = $(this).closest('.form-group').find('.datepicker-saved-value');
            container.on('changeDate', function(e) {
                var savedValue = e.format(0, '{$saveDateFormatJs}');
                hiddenInput.val(savedValue).trigger('change');
            }).on('clearDate', function(e) {
                var savedValue = e.format(0, '{$saveDateFormatJs}');
                hiddenInput.val(savedValue).trigger('change');
            });

            container.data('datepicker').update();
            container.data('datepicker')._trigger('changeDate');
        ";

        return $script;
    }
}

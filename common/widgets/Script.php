<?php

namespace common\widgets;

use Yii;
use yii\web\View;

class Script extends \yii\base\Widget
{
    public $position = View::POS_READY;


    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        $script = ob_get_clean();
        $script = preg_replace('|^\s*<script>|ui', '', $script);
        $script = preg_replace('|</script>\s*$|ui', '', $script);
        $this->getView()->registerJs($script, $this->position);
    }
}
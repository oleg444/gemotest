<?php

namespace common\base;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\UrlHelper;
use common\components\UnitOfWork\UnitOfWork;
use yii\base\InvalidValueException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * CrudController implements the CRUD actions
 */
abstract class CrudController extends Controller
{
    protected $modelClass = '';
    protected $searchModelClass = '';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all models corresponding to search filters.
     * @return mixed
     */
    public function actionIndex()
    {
        UrlHelper::remember();

        $searchModelClass = $this->searchModelClass;
        $searchModel = new $searchModelClass();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $unitOfWork = new UnitOfWork();
        $postData = Yii::$app->request->post();
        $modelClass = $this->modelClass;
        $model = new $modelClass();

        if ($postData && $this->loadModel($unitOfWork, $model, $postData)) {
            if ($unitOfWork->commit()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Cannot save record'));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $unitOfWork = new UnitOfWork();
        $postData = Yii::$app->request->post();
        $model = $this->findModel($id);

        if ($postData && $this->loadModel($unitOfWork, $model, $postData)) {
            if ($unitOfWork->commit()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Cannot save record'));
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page with saved search filters.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $unitOfWork = new UnitOfWork();
        $model = $this->findModel($id);

        $unitOfWork->registerDeleted($model);
        $unitOfWork->commit();

        return $this->redirect(UrlHelper::previous());
    }

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \yii\db\ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $modelClass = $this->modelClass;
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Loads model with relations from post data and registers all models in unit of work
     * @param $unitOfWork UnitOfWork
     * @param $model ActiveRecord
     * @param $postData array
     * @return bool
     */
    protected function loadModel(UnitOfWork $unitOfWork, ActiveRecord $model, $postData)
    {
        if ($model->load($postData)) {
            $model->validate();

            if ($model->isNewRecord) { $unitOfWork->registerNew($model); }
            else { $unitOfWork->registerChanged($model); }


            foreach ($postData[$model->formName()] as $field => $value) {
                if ($model->hasAttribute($field)) continue;  // already loaded


                $relationName = $field;
                $relationData = $value;

                $relation = $model->getRelation($relationName, false);
                if (!$relation) continue;

                if ($relationData === '') $relationData = [];
                if (!is_array($relationData)) continue;

                // handle many-to-many relations
                $manyToManyRelation = null;
                $manyToManyRelationName = '';
                if ($relation->via) {
                    if (!is_array($relation->via)) {
                        throw new InvalidValueException('Cannot determine relation class. Try to use via() method');
                    }

                    // change data for Model::load() method
                    $relatedFieldId = reset($relation->link);
                    foreach ($relationData as $index => $data) {
                        $relationData[$index] = [$relatedFieldId => $data];
                    }

                    $manyToManyRelation = $relation;
                    $manyToManyRelationName = $relationName;
                    $relationName = $relation->via[0];
                    $relation = $relation->via[1];
                }


                $relatedModels = [];
                $modelClass = $relation->modelClass;
                $relatedPrimaryKey = $modelClass::primaryKey();
                $relatedPrimaryKey = array_combine($relatedPrimaryKey, $relatedPrimaryKey);
                $modelField = reset($relation->link);
                $relatedModelField = key($relation->link);


                $currentRelatedModels = [];
                $indexField = '';
                if (!$model->isNewRecord) {
                    unset($relatedPrimaryKey[$relatedModelField]);
                    $indexField = reset($relatedPrimaryKey);
                    $currentRelatedModels = ArrayHelper::index($model->$relationName, $indexField);
                }
                foreach ($relationData as $index => $data) {
                    if (!$model->isNewRecord) {
                        $filter = ArrayHelper::filter($data, $relatedPrimaryKey);

                        /** @var ActiveRecord $relatedModel */
                        if ($filter) {
                            $relatedModel = ArrayHelper::remove($currentRelatedModels, $filter[$indexField]);
                            if ($relatedModel == null) {
                                $relatedModel = new $modelClass();
                            }
                        } else {
                            $relatedModel = new $modelClass();
                        }
                    } else {
                        $relatedModel = new $modelClass();
                    }



                    if ($relation->indexBy) {
                        $indexByField = $relation->indexBy;
                        $relatedModels[$relatedModel->$indexByField] = $relatedModel;
                    } else {
                        $relatedModels[] = $relatedModel;
                    }

                    if ($relatedModel->load($data, '')) {
                        $relatedModel->validate();


                        $unitOfWork->registerRelation($relatedModel, $relatedModelField, $model, $modelField);

                        if ($relatedModel->isNewRecord) { $unitOfWork->registerNew($relatedModel); }
                        else { $unitOfWork->registerChanged($relatedModel); }
                    }
                }

                // delete rest of models
                foreach ($currentRelatedModels as $modelToDelete) {
                    $unitOfWork->registerDeleted($modelToDelete);
                }


                $model->populateRelation($relationName, $relatedModels);

                if ($manyToManyRelation) {
                    $manyToManyModels = [];
                    foreach ($relatedModels as $relatedModel) {
                        $field = reset($manyToManyRelation->link);
                        $modelClass = $manyToManyRelation->modelClass;
                        $mmModel = $modelClass::findOne($relatedModel->$field);
                        if ($mmModel) {
                            if ($manyToManyRelation->indexBy) {
                                $indexByField = $manyToManyRelation->indexBy;
                                $manyToManyModels[$mmModel->$indexByField] = $mmModel;
                            } else {
                                $manyToManyModels[] = $mmModel;
                            }
                        }
                    }

                    $model->populateRelation($manyToManyRelationName, $manyToManyModels);
                }
            }

            return true;
        }

        return false;
    }
}

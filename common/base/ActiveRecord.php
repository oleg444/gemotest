<?php

namespace common\base;

use yii\db\ActiveRecord as BaseActiveRecord;

/**
 * Base class for models
 */
abstract class ActiveRecord extends BaseActiveRecord
{
}

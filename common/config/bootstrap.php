<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');

// these aliases can be overridden in main-local.php
Yii::setAlias('frontendUrl', '/');
Yii::setAlias('backendUrl', '/admin');

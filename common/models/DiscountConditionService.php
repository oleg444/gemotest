<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%discount_condition__service}}".
 *
 * @property integer $discount_condition_id
 * @property integer $service_id
 *
 * @property DiscountCondition $discountCondition
 * @property Service $service
 */
class DiscountConditionService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%discount_condition__service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discount_condition_id', 'service_id'], 'required'],
            [['discount_condition_id', 'service_id'], 'integer'],
            [['discount_condition_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiscountCondition::className(), 'targetAttribute' => ['discount_condition_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'discount_condition_id' => Yii::t('app', 'Discount Condition'),
            'service_id' => Yii::t('app', 'Service'),

            'discountCondition.name' => Yii::t('app', 'Discount Condition'),
            'service.name' => Yii::t('app', 'Service'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountCondition()
    {
        return $this->hasOne(DiscountCondition::className(), ['id' => 'discount_condition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * Returns string representation of entity name
     *
     * @return string
     */
    public function getName()
    {
        return $this->discount_condition_id;
    }

    /**
     * Returns list of entities as id => name
     *
     * @return array
     */
    public static function getList()
    {
        $models = self::find()->all();
        $list = ArrayHelper::map($models, 'discount_condition_id', 'name');
        return $list;
    }
}

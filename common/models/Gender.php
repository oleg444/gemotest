<?php

namespace common\models;

use Yii;

/**
 * Represents gender types
 */
class Gender
{
    const MALE = 'M';
    const FEMALE = 'F';

    /**
     * Returns list of entities as id => name
     *
     * @return array
     */
    public static function getList()
    {
        return [self::MALE => Yii::t('app', 'Male'), self::FEMALE => Yii::t('app', 'Female')];
    }

    /**
     * Returns gender name
     *
     * @param $gender
     * @return mixed|null
     */
    public static function getGenderName($gender)
    {
        $list = self::getList();
        return (isset($list[$gender]) ? $list[$gender] : null);
    }
}

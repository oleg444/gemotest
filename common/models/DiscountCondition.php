<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use DateTimeImmutable;
use DateInterval;

/**
 * This is the model class for table "{{%discount_condition}}".
 *
 * @property integer $id
 * @property integer $birth_date_type_id
 * @property boolean $is_phone_filled
 * @property string $phone_last_digits
 * @property string $gender
 * @property string $active_date_from
 * @property string $active_date_to
 * @property string $discount
 * @property boolean $is_active
 *
 * @property BirthDateType $birthDateType
 * @property DiscountConditionService[] $discountConditionServices
 * @property Service[] $services
 */
class DiscountCondition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%discount_condition}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active_date_from', 'discount'], 'required'],
            [['birth_date_type_id'], 'integer'],
            [['is_phone_filled', 'is_active'], 'boolean'],
            [['active_date_from', 'active_date_to'], 'safe'],
            [['gender'], 'string', 'max' => 1],
            [['discount'], 'string', 'max' => 20],
            [['phone_last_digits'], 'string', 'max' => 30],
            [['birth_date_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BirthDateType::className(), 'targetAttribute' => ['birth_date_type_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'birth_date_type_id' => Yii::t('app', 'Birth Date Type'),
            'is_phone_filled' => Yii::t('app', 'Is Phone Filled'),
            'phone_last_digits' => Yii::t('app', 'Phone Last Digits'),
            'gender' => Yii::t('app', 'Gender'),
            'active_date_from' => Yii::t('app', 'Active Date From'),
            'active_date_to' => Yii::t('app', 'Active Date To'),
            'discount' => Yii::t('app', 'Discount'),
            'is_active' => Yii::t('app', 'Is Active'),

            'birthDateType.name' => Yii::t('app', 'Birth Date Type'),
            'services' => Yii::t('app', 'Services'),
            'genderName' => Yii::t('app', 'Gender'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBirthDateType()
    {
        return $this->hasOne(BirthDateType::className(), ['id' => 'birth_date_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountConditionServices()
    {
        return $this->hasMany(DiscountConditionService::className(), ['discount_condition_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])->via('discountConditionServices')->indexBy('id');
    }

    /**
     * Returns gender name by gender field value
     *
     * @return string
     */
    public function getGenderName()
    {
        return Gender::getGenderName($this->gender);
    }

    /**
     * Returns string representation of entity name
     *
     * @return string
     */
    public function getName()
    {
        return $this->id;
    }

    /**
     * Returns list of entities as id => name
     *
     * @return array
     */
    public static function getList()
    {
        $models = self::find()->all();
        $list = ArrayHelper::map($models, 'id', 'name');
        return $list;
    }

    /**
     * Returns true if discount is with percent, false if discount is absolute
     *
     * @return int
     */
    public function isPercent()
    {
        return (substr_compare($this->discount, '%', strlen($this->discount) - 1) === 0);
    }

    /**
     * Get order discount
     *
     * @param $order
     * @return int|null
     */
    public function getOrderDiscount($order)
    {
        if ($this->match($order)) {
            return (int)$this->discount;
        }
        return null;
    }

    /**
     * Check match
     *
     * @param $order
     * @return bool
     */
    public function match($order)
    {
        $match = $this->isActive()
            && $this->matchActiveDate()
            && $this->matchServices($order)
            && $this->matchBirthDate($order)
            && $this->matchIsPhoneFilled($order)
            && $this->matchPhoneLastDigits($order)
            && $this->matchGender($order)
        ;

        return $match;
    }

    /**
     * Check active
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active;
    }

    /**
     * Check match active date
     * @return bool
     */
    public function matchActiveDate()
    {
        $dateFrom = ($this->active_date_from ? new DateTimeImmutable($this->active_date_from) : null);
        $dateTo = ($this->active_date_to ? new DateTimeImmutable($this->active_date_to) : null);
        $now = new DateTimeImmutable();
        return ((is_null($dateFrom) || $dateFrom <= $now) && (is_null($dateTo) || $dateTo >= $now));
    }

    /**
     * Check match services
     *
     * @param Order $order
     * @return bool
     */
    public function matchServices(Order $order)
    {
        $orderServiceIds = array_keys($order->services);
        $conditionServiceIds = array_keys($this->services);
        $sameServiceIds = array_intersect($orderServiceIds, $conditionServiceIds);
        return ($sameServiceIds == $conditionServiceIds);
    }

    /**
     * Check match birthday date
     * @param Order $order
     * @return bool
     */
    public function matchBirthDate(Order $order)
    {
        if (empty($this->birth_date_type_id)) {
            return true;
        }

        $realBirthDate = new DateTimeImmutable($order->birth_date);
        $str = date('Y-') . $realBirthDate->format('m-d');
        $currentYearBirthDate = new DateTimeImmutable($str);

        $today = new DateTimeImmutable(date('Y-m-d'));
        $oneWeekBeforeBirthDate = $currentYearBirthDate->sub(new DateInterval('P1W'));
        // $today устанавливается на начало дня, а день рождения действует в течение всего дня
        // поэтому прибавляем 1 день
        $oneWeekAfterBirthDate = $currentYearBirthDate->add(new DateInterval('P1W1D'));

        if ($this->birth_date_type_id == BirthDateType::ONE_WEEK_BEFORE) {
            return ($today >= $oneWeekBeforeBirthDate && $today <= $currentYearBirthDate);
        }

        if ($this->birth_date_type_id == BirthDateType::ONE_WEEK_AFTER) {
            return ($today >= $currentYearBirthDate && $today <= $oneWeekAfterBirthDate);
        }

        if ($this->birth_date_type_id == BirthDateType::ONE_WEEK_BEFORE_AND_AFTER) {
            return ($today >= $oneWeekBeforeBirthDate && $today <= $oneWeekAfterBirthDate);
        }

        return false;
    }

    /**
     * Check match phone field
     *
     * @param Order $order
     * @return bool
     */
    public function matchIsPhoneFilled(Order $order)
    {
        return (empty($this->is_phone_filled) || !empty($order->phone));
    }

    /**
     * Check match phone last digits
     *
     * @param Order $order
     * @return bool
     */
    public function matchPhoneLastDigits(Order $order)
    {
        return (
            empty($this->phone_last_digits)
            || (
                substr(
                    preg_replace('/[^\d]/', '', $order->phone),
                    -($length = strlen($this->phone_last_digits)),
                    $length
                ) == $this->phone_last_digits
            )
        );
    }

    /**
     * Check match gender
     * @param Order $order
     * @return bool
     */
    public function matchGender(Order $order)
    {
        return (empty($this->gender) || $order->gender == $this->gender);
    }
}

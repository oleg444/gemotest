<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%birth_date_type}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DiscountCondition[] $discountConditions
 */
class BirthDateType extends \yii\db\ActiveRecord
{
    const ONE_WEEK_BEFORE = 1;
    const ONE_WEEK_AFTER = 2;
    const ONE_WEEK_BEFORE_AND_AFTER = 3;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%birth_date_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountConditions()
    {
        return $this->hasMany(DiscountCondition::className(), ['birth_date_type_id' => 'id']);
    }

    /**
     * Returns string representation of entity name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns list of entities as id => name
     *
     * @return array
     */
    public static function getList()
    {
        $models = self::find()->all();
        $list = ArrayHelper::map($models, 'id', 'name');
        return $list;
    }
}

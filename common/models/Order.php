<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property string $fio
 * @property string $birth_date
 * @property string $phone
 * @property string $gender
 * @property integer $discount_condition_id
 * @property string $created_at
 *
 * @property OrderService[] $orderServices
 * @property Service[] $services
 * @property DiscountCondition $discountCondition
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'birth_date'], 'required'],
            [['birth_date', 'phone', 'gender'], 'safe'],
            [['fio'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 30],
            [['gender'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'TimestampBehavior' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => function () { return date('Y-m-d H:i:s'); },
                'updatedAtAttribute' => null,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fio' => Yii::t('app', 'Fio'),
            'birth_date' => Yii::t('app', 'Birth Date'),
            'phone' => Yii::t('app', 'Phone'),
            'gender' => Yii::t('app', 'Gender'),
            'created_at' => Yii::t('app', 'Created At'),
            'genderName' => Yii::t('app', 'Gender'),
            'services' => Yii::t('app', 'Services'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderServices()
    {
        return $this->hasMany(OrderService::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])->via('orderServices')->indexBy('id');
    }

    /**
     * Returns gender name by gender field value
     * @return string
     */
    public function getGenderName()
    {
        return Gender::getGenderName($this->gender);
    }

    /**
     * Returns string representation of entity name
     * @return string
     */
    public function getName()
    {
        return $this->id;
    }

    /**
     * Returns list of entities as id => name
     * @return array
     */
    public static function getList()
    {
        $models = self::find()->all();
        $list = ArrayHelper::map($models, 'id', 'name');
        return $list;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountCondition()
    {
        return $this->hasOne(DiscountCondition::className(), ['id' => 'discount_condition_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $discountCondition = $this->calcDiscountCondition();
            $this->discount_condition_id = ($discountCondition ? $discountCondition->id : null);
        }

        return parent::beforeSave($insert);
    }

    /**
     * Get discount
     *
     * @return mixed|null|string
     */
    public function getDiscount()
    {
        if ($this->discount_condition_id) {
            return $this->discountCondition->discount;
        } elseif ($this->isNewRecord) {
            $condition = $this->calcDiscountCondition();
            if ($condition) {
                return $condition->discount;
            }
            return null;
        }

        return null;
    }

    /**
     * Calculate discount condition
     *
     * @return string|\yii\db\ActiveRecord
     */
    public function calcDiscountCondition()
    {
        $conditions = DiscountCondition::find()
            ->where(['is_active' => 1])
            ->indexBy('id')
            ->all();

        $discounts = array_map(function (DiscountCondition $condition) {
            return ['isPercent' => $condition->isPercent(), 'discount' => $condition->getOrderDiscount($this)];
        }, $conditions);

        $discounts = array_filter($discounts, function ($discountInfo) {
            return ($discountInfo['discount'] !== null);
        });

        if (!$discounts) {
            return '';
        }

        uasort($discounts, function ($a, $b) {
            if ($a['isPercent'] > $b['isPercent']) return 1;
            if ($a['isPercent'] < $b['isPercent']) return -1;
            if ($a['discount'] < $b['discount']) return 1;
            if ($a['discount'] > $b['discount']) return -1;
            return 0;
        });

        $discount_condition_id = key($discounts);

        return $conditions[$discount_condition_id];
    }
}
